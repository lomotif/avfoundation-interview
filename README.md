Remix Video:
---
Using `events.csv`, create a 720p 30fps remixed video output where the columns represents the following:

- 1st col, t - timestamp of UI event  
- 2nd col, v - normalized video timeline [0.0, 1.0] which represents the first 4.0s of the video `test.mov`

This essentially means at t0, the video should show the content present at v0, and continue to play till v1 for t1-t0 seconds.

eg.  
0.0, 0.0  
2.0, 1.0  
10.0, 0.5  


Remixed video should play content from 0.0s to 4.0s over a duration of 2 secs (sped up), followed by content from 4.0s to 2.0s (backwards) over a duration of the next 8 secs (slow down).





Hint:
---
1. need to create a reverse video
2. figure out how to interleave the normal and reverse video
3. need to take care of situations where the video content remains still
